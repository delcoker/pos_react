import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import _ from 'lodash';
import {PubSub} from 'graphql-subscriptions';
import {requiresAdmin, requiresAuth} from './permissions';

import {del_seed} from "./del_seed"
import {default as sequelize, Op, where} from "sequelize";

export const pubsub = new PubSub();

const USER_ADDED = 'USER_ADDED';

export default {
    Subscription: {
        userAdded: {
            subscribe: () => pubsub.asyncIterator(USER_ADDED),
        },
    },
    Item: { // how an item's category and vendor fields are obtained
        category: (parent, args, {models}) =>
            models.ItemCategory.findOne({
                where: {
                    id: parent.category_id,
                }
            }),
        vendor: (parent, args, {models}) =>
            models.Vendor.findOne({
                where: {
                    id: parent.vendor_id,
                }
            }),
    },
    ItemCategory: {
        vendor: (parent, args, {models}) => models.Vendor.findOne({
            where: {
                id: parent.vendor_id,
            }
        }),
    },
    Transaction: {
        user: ({user_id}, args, {models}) =>
            models.User.findOne({
                where: {
                    user_id
                }
            }),
        transaction_point: ({id}, args, {models}) =>
            models.TransactionPoint.findOne({
                where: {
                    transaction_point_id: id
                }
            }),
        vendor: (parent, args, {models}) => models.Vendor.findOne({
            where: {
                id: parent.vendor_id,
            }
        }),
        // transactionDetails: (parent, args, {models}) => // parent apparently refers to Transaction
        //     models.TransactionDetail.findAll({
        //         where: {
        //             transaction_id: parent.id,
        //         }
        //     }),
        transactionDetails: ({id}, args, {transactionDetailLoader}) => {// parent apparently refers to Transaction
            console.log('iddddddddddd', transactionDetailLoader);
            console.log('iddddddddddd', id);
            return transactionDetailLoader.load(id)
        }
    },
    TransactionDetail: {
        item: (parent, args, {models}) => // parent apparently refers to TransactionDetail
            models.Item.findOne({
                where: {
                    id: parent.item_id,
                }
            }),
    },
    TransactionPoint: {
        vendor: (parent, args, {models}) => models.Vendor.findOne({
            where: {
                id: parent.vendor_id,
            }
        }),
    },
    User: {
        vendor: (parent, args, {models}) => models.Vendor.findOne({
            where: {
                id: parent.vendor_id,
            }
        }),
        email: (parent, args, {models}) => {
            // console.log(parent);
            return models.LocalAuth.findOne({

                where: {
                    user_id: parent.user_id,
                }
            })
        },
    },
    Shift: {
        shift_details: (parent, args, {models}) => {

            // console.log(';adsf');
            return models.ShiftDetail.findAll({

                where: {
                    shift_id: parent.id,
                }
            });
        },
        user: (parent, args, {models}) => {

            // console.log(';adsf');
            return models.User.findOne({
                where: {
                    user_id: parent.user_id,
                }
            });
        },
    },
    ShiftDetail: {
        // shift: (parent, args, {models}) => {
        //
        //     return models.Shift.findOne({
        //
        //         where: {
        //             id: parent.shift_id,
        //         }
        //     });
        // },
        item: (parent, args, {models}) =>
            models.Item.findOne({ // should be finDAll batch transaction
                where: {
                    id: parent.item_id,
                },
            }),
    },

    Query: {
        allUsers: requiresAdmin.createResolver((parent, args, {models}) => models.User.findAll()),

        allUsersNoAdmin: ((parent, args, {models}) => models.User.findAll( // enabled
            // for drop-down at login
            {
                where: {status: 'enabled'},
                order:
                    [['first_name', 'ASC']],
            }
        )),

        allUsersNoAdminDis: ((parent, args, {models}) => models.User.findAll( // all users
            {
                where: {//-------------- enable this del todo
                    [Op.or]: [{status: 'enabled'}, {status: 'disabled'}]
                },
                order:
                    [['first_name', 'ASC']],
            }
        )),
        // email: ((parent, args, {models}) => models.LocalAuth.findAll()),

        getAllItems: ((parent, args, {models}) => models.Item.findAll(
            {
                where: {
                    [Op.or]: [{status: 'enabled'}, {status: 'disabled'}]
                },
                order:
                    [['name', 'ASC']],
            }
        )),
        getItem: ((parent, args, {models}) => models.Item.findOne(
            {
                where: {
                    id: args.id
                }
            }
        )),
        getEnabledItems: ((parent, args, {models}) => models.Item.findAll(
            {
                order:
                    [['name', 'ASC']],
                where: {
                    status: "enabled"
                }
            }
        )),
        getItemCategories: ((parent, args, {models}) => models.ItemCategory.findAll(
            {
                order:
                    [['name', 'ASC']],
            }
        )),

        me: (parent, args, {models, user}) => {
            // console.log('user', user);
            // console.log(parent);
            if (user) {
                //logged in
                return models.User.findOne({

                    where: {
                        user_id: user.user_id,
                    }
                });
            }
        },

        getAllTransactions: (parent, args, {models}) => {
            let created_at = new Date();
            if (args.endDate === args.startDate) {
                created_at = args.startDate
            } else {
                created_at = {
                    [Op.between]: [args.startDate, args.endDate],
                }
            }
            /*
                        console.log('fidAllllllllllllllllllllllllllll')
                        console.log(models.Transaction.findAll({
                            where: {
                                user_id: args.user_id,
                                createdAt: created_at
                            }
                        }))
                        console.log('fidAllllllllllllllllllllllllllll')
                        */

            return models.Transaction.findAll({
                where: {
                    user_id: args.user_id,
                    createdAt: created_at
                }
            })
        },

        getDailyReport: async (parent, args, {models}) => {
            // this was a disaster
            // let created_at = new Date();
            // if (args.endDate === args.startDate) {
            //     created_at = args.startDate
            // } else {
            //     created_at = {
            //         [Op.between]: [args.startDate, args.endDate],
            //     }
            // }
            let whereStatement = {
                user_id: args.user_id,
                createdAt: {
                    [Op.between]: [args.startDate, args.endDate],
                }
            };
            let attributeStatement =
                ['user_id', 'vendor_id', /*'createdAt', 'updatedAt',*/];

            let groupByStatement =
                [/*'transaction_detail_s.item_id',*/ 'transaction_detail_s->item_.id', 'transaction_detail_s->item_"."name', 'user_id', 'transaction_.vendor_id', 'transaction_detail_s->item_->item_category_.id', 'transaction_detail_s.item_.quantity'];


            // don't select user and vendor if you want everyone
            if (args.user_id === -1) {
                attributeStatement = ['vendor_id', /*'createdAt', 'updatedAt',*/];

                whereStatement = {
                    createdAt: {
                        [Op.between]: [args.startDate, args.endDate],
                    }
                };

                groupByStatement =
                    ['transaction_detail_s->item_.id', 'transaction_detail_s->item_"."name', 'transaction_.vendor_id', 'transaction_detail_s->item_->item_category_.id'];
            }

            let result = await (models.Transaction.findAll({
                attributes: attributeStatement,
                where: whereStatement,
                include: [
                    {
                        model: models.TransactionDetail,
                        attributes: [
                            // "item_id",
                            [sequelize.fn('sum', sequelize.col('transaction_detail_s.quantity')), 'qty_sold'],

                        ],
                        include: [
                            {
                                model: models.Item,
                                attributes: ["name", 'price', 'quantity'],
                                include: [
                                    {
                                        model: models.ItemCategory,
                                        attributes: ["name",],
                                    }
                                ]
                            }
                        ]
                    },
                    // { // transaction_point_ is not associated to transaction_
                    //     model:models.TransactionPoint,
                    //     attributes:['id', 'name']
                    // }
                ],
                group: groupByStatement,
                // order: // doesn't work
                //     [['name', 'ASC']],
                raw: true,
            }));
            // console.log('resssssssssssssult', result);

            let res_refined = [];
            result.map((k) => {
                res_refined.push({
                    user_id: k.user_id,
                    vendor_id: k.vendor_id,
                    item_name: k['transaction_detail_s.item_.name'],
                    item_category: k['transaction_detail_s.item_.item_category_.name'],
                    item_price: parseFloat(k['transaction_detail_s.item_.price']),
                    qty_sold: parseInt(k['transaction_detail_s.qty_sold']),
                    item_id: k['transaction_detail_s.item_.id'],
                    inv: k['transaction_detail_s.item_.quantity'],
                    transaction_point: 'Working on It'
                });
            });

            // console.log('res.dataValuesssssssssssssssss', res_refined);

            return res_refined;

        },

        getShiftDetails: ((parent, args, {models}) => {
            console.log('parent', parent);
            console.log('args', args);
            console.log('models', models);

            return models.Item.findAll({
                where: {shift_id: args.shift_id},
            });
        }),

        getShift: (parent, args, {models}) => {
            let whereStatement = {
                createdAt: {
                    [Op.between]: [args.startDate, args.endDate],
                }
            };
            if (args.status) {
                whereStatement.status = args.status;
            }
            if (args.user_id > 0) { // all users
                whereStatement.user_id = args.user_id;
            }

            return models.Shift.findAll({
                where: whereStatement,
            });
        },

        userShiftStarted: async (parent, args, {models}) => {

            let whereStatement = {
                user_id: args.user_id,
                status: 'enabled',
                // createdAt: {
                //     [Op.between]: [args.startDate, args.endDate],
                // }
            };
            if (args.user_id === -1) { // get all active shifts
                // attributeStatement = ['vendor_id', /*'createdAt', 'updatedAt',*/];
                whereStatement = {
                    status: 'enabled',
                    // createdAt: {
                    //     [Op.between]: [args.startDate, args.endDate],
                    // }
                };
            }
            return await models.Shift.findOne({
                // attributes: ['id', 'user_id', 'status'],
                where: whereStatement,
            }) !== null;

        },

        isOneShiftActive: (parent, args, {models}) => {

            return models.Shift.findOne({
                // attributes: ['id', 'user_id', 'status'],
                where: {status: 'enabled'}
            });

        },

        // validPass: async (parent, args, {models}) => {
        //
        //     const valid = await bcrypt.compare(password, user.password);
        //
        //     return models.LocalAuth.findOne({
        //         // attributes: ['id', 'user_id', 'status'],
        //         where: {status: 'enabled'}
        //     });
        //
        // },
    },

    Mutation: {
        createDummyUsers: (async (parent, args, {models}) => { // just to seed
            if (args.pass === '123') {
                await del_seed();
                return 'successful seed';
            }
            return '0'
        }),

        createUser: requiresAdmin.createResolver(async (parent, args, {models}) => {
            // console.log(args);
            const user = _.pick(args, ['first_name', 'last_name', 'other_names', 'isAdmin', 'vendor_id', 'status', 'telephone', 'pic', 'postal_address']);
            const localAuth = _.pick(args, ['email', 'password',]);
            const passwordPromise = bcrypt.hash(localAuth.password, 12);
            // console.log(localAuth);
            //before you create a new user check this del
            const new_user_valid = await models.LocalAuth.findOne({
                where: {email: args.email}
            });
            if (new_user_valid) {
                console.log("Email already exists");
                console.log(user);
                user.message = "Email already exists";
                user.user_id = -1;
                return user;
            }
            //
            // console.log(localAuth);

            const createUserPromise = models.User.create(user);
            // console.log(createUserPromise);
            const [password, createdUser] = await Promise.all([passwordPromise, createUserPromise]);
            // console.log(password);
            localAuth.password = password;

            pubsub.publish(USER_ADDED, {
                // userAdded,
            });

            // console.log(localAuth);

            return models.LocalAuth.create({
                ...localAuth,
                user_id: createdUser.user_id,
            }, /*userAdded*/);

            /*
            ////////////// old
            const user = args;
            // user.password = await bcrypt.hash(user.password, 12);
            const userAdded = await models.User.create(user);
             */

            // return userAdded;
        }),

        updateUser: requiresAdmin.createResolver((parent, {user_id, first_name, last_name, other_names, status, vendor_id, postal_address, telephone, pic, isAdmin}, {models}) =>
            models.User.update({
                first_name: first_name,
                last_name: last_name,
                other_names: other_names,
                postal_address: postal_address,
                telephone: telephone,
                status: status,
                vendor_id: vendor_id,
                pic: pic,
                isAdmin,
            }, {
                where: {user_id}
            })),

        updateItem: requiresAdmin.createResolver(async (parent, args, {models}) => {//console.log(args);

            if (args.id === 0) { /// save new item

                let item = await models.Item.findOne({
                    where: {
                        name: args.name,
                        // [Op.ne]: [{status: 'enabled'}],
                        [Op.or]: [{status: 'enabled'}, {status: 'disabled'}]
                    }
                });


                if (item) {
                    // console.log('item with this name', item);
                    return item.dataValues;
                }

                let a = await (models.Item.create({
                    name: args.name,
                    price: args.price,
                    category_id: args.category_id,
                    pic: args.pic,
                    has_stock: args.has_stock,
                    quantity: args.quantity,
                    status: args.status,
                    available: args.available,
                    vendor_id: args.vendor_id,
                    min_stock_level: args.min_stock_level,
                }));
                // console.log('a', a.dataValues);
                return a.dataValues;
            }

            // console.log(args);
            let a = await models.Item.update({
                name: args.name,
                price: args.price,
                category_id: args.category_id,
                pic: args.pic,
                has_stock: args.has_stock,
                quantity: args.quantity,
                status: args.status,
                available: args.available,
                vendor_id: args.vendor_id,
                min_stock_level: args.min_stock_level,
            }, {
                where: {id: args.id}
            });
            return models.Item.findOne({
                where: {id: args.id}
            });

        }),

        updateStock: requiresAdmin.createResolver(async (parent, args, {models}) => {
            // console.log(args);
            // first get the quantity currently in stock and add to that value
            const item = await models.Item.findOne({
                where: {id: args.id}
            });

            // if a cashier is on a shift, add to qty_during_shift
            const shift = await models.Shift.findOne({
                attributes: ['id', 'user_id', 'status'],
                where: {status: 'enabled'}
            });
            // console.log(shift);
            if (shift) { // if one session is active
                await models.ShiftDetail.update({
                        qty_during_shift: args.qty_to_add
                    },
                    {
                        where: {
                            shift_id: shift.dataValues.id,
                            item_id: item.dataValues.id
                        }
                    });
            }
            //

            const old_item_qty = item.quantity;
            const new_item_qty = old_item_qty + args.qty_to_add;

            // console.log(old_item_qty, new_item_qty);

            models.Item.update({
                    quantity: new_item_qty
                },
                {
                    where: {id: args.id}
                });

            // then add to inventory table
            models.Inventory.create({
                quantity_added: args.qty_to_add,
                quantity_after_this_addition: new_item_qty,
                user_id: args.user_id,
                item_id: args.id
            });

            return [old_item_qty, new_item_qty];
        }),

        deleteUser: requiresAdmin.createResolver((parent, args, {models}) => models.User.destroy({where: args})),

        createVendor: (parent, args, {models}) => models.Vendor.create(args),

        register: async (parent, args, {models}) => {
            // console.log(args);
            const user = _.pick(args, ['first_name', 'last_name', 'other_names', 'isAdmin', 'vendor_id', 'status']);
            const localAuth = _.pick(args, ['email', 'password']);
            const passwordPromise = bcrypt.hash(localAuth.password, 12);
            // console.log(localAuth);
            //before you create a new user check this del
            const new_user_valid = await models.LocalAuth.findOne({
                where: {email: args.email}
            });
            if (new_user_valid) {
                console.log("Email already exists");
                console.log(user);
                user.message = "Email already exists";
                user.user_id = -1;
                return user;
            }
            //

            const createUserPromise = models.User.create(user);
            // console.log(createUserPromise);
            const [password, createdUser] = await Promise.all([passwordPromise, createUserPromise]);
            // console.log(password);
            localAuth.password = password;

            // const createdUser = await models.User.create(user);
            return models.LocalAuth.create({
                ...localAuth,
                user_id: createdUser.user_id,
            });
        },

        resetPasswordAdmin: requiresAdmin.createResolver(async (parent, args, {models}) => {

            let password = await bcrypt.hash('123', 12);
            if (args.user_id === 1) {
                password = await bcrypt.hash('62131121', 12);
            }
            const a = await models.LocalAuth.update({
                    password: password
                },
                {
                    where: {
                        id: args.user_id
                    }
                });
            // console.log('saaaaaaaaaaaaaaa', a[0]);
            // return a !== null;
            // return !!a; // a ? true : false;
            return a[0]; // a ? true : false;
        }),

        changePassword: requiresAuth.createResolver(async (parent, args, {models}) => {

            const user = await models.LocalAuth.findOne({where: {id: args.user_id}});
            const valid = await bcrypt.compare(args.old, user.password);

            if (!valid) throw new Error("Incorrect Old Password");

            if(args.newP !== args.conf) throw new Error("New password does not match confirm.");

            const newPass = await bcrypt.hash(args.newP, 12);

            const a = await models.LocalAuth.update({
                    password: newPass
                },
                {
                    where: {
                        id: args.user_id
                    }
                });

            return a[0]; // a ? true : false;
        }),

        login: async (parent, {email, password}, {models, SECRET}) => {
            const user = await models.LocalAuth.findOne({where: {email}});
            const userDetails = await models.User.findOne({where: {user_id: user.user_id}});

            // console.log('this.userrrr```````=----', user);
            // console.log('this.userrrr```````=----', userDetails);

            if (!user) {
                throw new Error("No user with that email");
            }
            const valid = await bcrypt.compare(password, user.password);
            if (!valid) {
                throw new Error("Incorrect Password");
            }

            user.isAdmin = userDetails.dataValues.isAdmin;

            // token = '12341324134rfd4354234543ere45434rr
            // verify = needs secret | used for authentication
            // decode = no secret needed | use on the client side

            const token = jwt.sign({
                    user: _.pick(user, ['user_id', 'email', 'isAdmin']),
                },
                SECRET,
                {
                    expiresIn: '1d'
                }
            );
            // console.log(token);
            return token;
        },

        saveTransaction: async (parent, args, {models}) => {
            // console.log('saveTransaction',args)
            //////////////////////////////// check if quantity being bought is less than what's left

            let ii = 0;
            for (const id of args.item_ids) {
                // find every item and reduce the quantity by that amount
                const item = await models.Item.findOne({
                    where: {id: id, has_stock: true}
                });

                // there will be a value lower than 0
                if (item && item.quantity - args.qtys[ii] < 0) {
                    return 0;
                }
            }

            // console.log("sdddddddddddddddddddddddddddddsdfsd");

            ///////////////////////////////////////////////////////////////////
            let data = await models.Transaction.create({
                total_amount: args.total_amount,
                payment_type: args.payment_type,
                payment_method: args.payment_method,
                user_id: args.user_id,
                // transaction_point_id: args.transaction_point_id,
                vendor_id: args.vendor_id
            });

            if (!data) {
                throw new Error("Transaction not saved");
            }

            let i = 0;
            for (const id of args.item_ids) {
                models.TransactionDetail.create({
                    quantity: args.qtys[i],
                    item_id: id,
                    transaction_id: data.dataValues.id
                });

                // find every item and reduce the quantity by that amount
                const item = await models.Item.findOne({
                    where: {id: id}
                });

                // console.log(item);

                if (item.has_stock) {
                    const old_item_qty = item.quantity;
                    const new_item_qty = old_item_qty - args.qtys[i];

                    // console.log(old_item_qty, new_item_qty);

                    models.Item.update({
                            quantity: new_item_qty
                        },
                        {
                            where: {id: id}
                        });
                }
                ///////////////////////////////////////////////

                i++;
            }

            // console.log('heeeeeeeeeeeeeeeeeeeeeeeeeeeee', data.dataValues);

            return data.dataValues.id
        },

        createShift: async (parent, args, {models}) => {

            // check if user already has active section (at transaction point)
            const has_shift = await models.Shift.findOne({
                where: {
                    user_id: args.user_id,
                    status: 'enabled'
                }
            });

            // console.log('has_shift', has_shift);

            if (has_shift) { // a shift already exists
                return false;
                // return has_shift;
            }

            const shift = await models.Shift.create({
                user_id: args.user_id,
                status: 'enabled'
            });

            // get aLL items ... note: what if an items.has_stock is enabled during a shift
            const items = await models.Item.findAll({
                where: {
                    has_stock: true,
                    status: 'enabled'
                }
            });
            // console.log("shift.dataValues.id)", shift.dataValues.id);
            // return(items.dataValues.id);

            for (const item of items) { // copy qtys
                models.ShiftDetail.create({
                    shift_id: shift.dataValues.id,
                    item_id: item.dataValues.id,
                    qty_start: item.dataValues.quantity
                });
            }

            return true;
            // return shift;
        },

        endShift: async (parent, args, {models}) => {

            // check if user already has active section (at transaction point)
            const has_shift = await models.Shift.findOne({
                where: {
                    user_id: args.user_id,
                    status: 'enabled'
                }
            });

            // console.log('has_shift', has_shift);
            if (!has_shift) return false;

            // let shift_update = -1;
            // if (has_shift) { // if a shift exists
            await models.Shift.update({
                    status: 'disabled'
                },
                {
                    where: {
                        id: has_shift.dataValues.id,
                    }
                });


            // get aLL items ...
            // note: what if an items.has_stock is enabled during a shift - find all missing items from the shift_details and add it.... or when an item is enabled, check what sessions are active and add the missing item
            const items_in_shift = await models.ShiftDetail.findAll({
                where: {shift_id: has_shift.dataValues.id,},
                include: [
                    {
                        model: models.Item,
                        required: false
                    }
                ]
            });

            if (!items_in_shift) return false;

            // console.log('items_in_shift', items_in_shift);
            // console.log('items_in_shift', );
            // console.log('items_in_shift', getShiftDetails(parent, has_shift.dataValues.id, models));
            // return items_in_shift;
            // console.log('tttttttttttt', items_in_shift);
            // console.log('aaaaaaaaaaaaa', items_in_shift[0].dataValues);
            // console.log('xxxxxxxxxxx', items_in_shift[0].dataValues.item_.id);
            // return true;

            for (const s_item of items_in_shift) { // copy qtys
                // console.log('has_shift', has_shift);
                // console.log('zzzzzzzzzzzzzzzzzzzzzzzzzzzzzz',s_item.dataValues.item_.id);
                models.ShiftDetail.update({
                        qty_end: s_item.dataValues.item_.quantity
                    },
                    {
                        where: {
                            shift_id: has_shift.dataValues.id,
                            item_id: s_item.dataValues.item_.id,
                        }
                    });
            }
            return true;
        },

    },
};
