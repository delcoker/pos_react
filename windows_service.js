var Service = require('node-windows').Service;

// Create a new service object
var svc = new Service({
    name:'AKORNO POS SERVER',
    description: 'The nodejs.org example web server.',
    script: 'C:\\Users\\delco\\Documents\\pos_react_server\\start_server.bat'
});

// Listen for the "install" event, which indicates the
// process is available as a service.
svc.on('install',function(){
    svc.start();
});

svc.install();