export default `

    type Subscription {
        userAdded: User!
    }

    type Item{
        id: Int!
        name: String!
        price: Float!
        pic: String
        category: ItemCategory!
        has_stock: Boolean
        quantity: Int
        min_stock_level: Int
        vendor: Vendor!
        status: String!
        createdAt: String!
        updatedAt: String!
    }
    
    type ItemCategory{
        id: Int!
        name: String!
        vendor: Vendor!
        status: String!
        createdAt: String!
        updatedAt: String!        
    }
    
    type Transaction{
        id: Int!
        user: User
        total_amount: Float
        payment_type: String
        "subscriber_id: String"
        payment_method: String
        payment_detail: String
        transaction_point: TransactionPoint
        vendor: Vendor!
        createdAt: String!
        updatedAt: String!
        transactionDetails: [TransactionDetail!]!
    }
    
    type TransactionDetail{
        id: Int!
        "transaction: Transaction!"
        item: Item!
        quantity: Int!
        createdAt: String!
        updatedAt: String!
    }
    
    type TransactionPoint{
        id: Int!
        name: String
        vendor: Vendor!
        status: String!
        createdAt: String!
        updatedAt: String!
    }

    type Vendor{
        id:Int!
        name: String!
        email: String!
        telephone: String
        postal_address: String
        website_url: String
        status: String!
        createdAt: String!
        updatedAt: String!
    }

    type User{
        user_id:Int!
        first_name: String
        last_name: String
        other_names: String
        postal_address: String
        telephone: String
        pic: String
        email: LocalAuth!
        password: String "take this out del"
        vendor: Vendor!
        vendor_staff_perm_id: String
        isAdmin: Boolean
        status: Status
        createdAt: String!
        updatedAt: String!
    }
    
    type LocalAuth {
        id: Int!
        user_id:Int!
        email: String!
    }
    
    enum Status {
      enabled
      disabled
      deleted
    }
    
    type Shift{
        id: Int!
        user_id: Int!
        status: Status! "active, completed, error"
        createdAt: String!
        updatedAt: String!
        shift_details: [ShiftDetail]
        user: User
    }
    
    type ShiftDetail{
        id: Int!
        shift_id: Int!
        item: Item!
        qty_start: Int
        qty_during_shift: Int
        qty_end: Int
        createdAt: String!
        updatedAt: String!
    }

    type Report{
        user_id:Int
        vendor_id: Int
        item_name: String!
        item_category: String!
        item_price: Float!
        qty_sold: Int!
        item_id: Int!
        inv: String
        transaction_point: String
    }
    
    type Query{
        allUsers: [User!]!
        
        allUsersNoAdmin: [User!]!
        
        allUsersNoAdminDis: [User!]!
        
        getItem(id:Int!): Item
        
        getAllItems: [Item]
        
        getEnabledItems: [Item]
        
        getItemCategories: [ItemCategory]
        
        me: User

        getAllTransactions(user_id:Int, startDate:String, endDate:String): [Transaction] "not using this"
        
        getDailyReport(user_id:Int, startDate:String!, endDate:String!, transactionPoint: Int): [Report] "should have returned [TransactionDetails] I think"
        
        getShiftDetails(shift_id:Int!): [ShiftDetail] "helper method for endShift mutation but not working"
        
        getShift(user_id:Int, startDate:String!, endDate:String!, transactionPoint: Int, status:String ): [Shift]
        
        userShiftStarted(user_id:Int): Boolean!
    
        isOneShiftActive: Shift
    }
    
    type Mutation{
    
        createDummyUsers(pass: String): String
    
        "createUser(first_name: String!, last_name:String!, other_names:String, email: String!, status: Status, vendor_id: Int): User"
        
        createUser(first_name: String!, last_name:String!, other_names:String, email: String!, password:String, status: Status, vendor_id: Int!, isAdmin:Boolean, telephone:String, pic:String, postal_address:String): User
        
        updateUser(pic:String, user_id: Int!,first_name: String, last_name:String, other_names:String, status: Status, vendor_id: Int!, isAdmin:Boolean, telephone:String, postal_address:String): [Int!]!
        
        deleteUser(user_id: Int!): Int!
        
        createVendor(name: String!, email:String!, telephone:String, postal_address:String, website_url: String, status: Status): Vendor
        
        "register(email: String!, password:String!, isAdmin:Boolean): User!"
        register(first_name: String!, last_name:String!, other_names:String, email: String!, password:String!, status: Status, vendor_id: Int!, isAdmin:Boolean): User
        
        login(email: String!, password: String!): String!
        
        updateItem(id: Int!, name: String, price: Float, category_id: Int,pic: String, has_stock:Boolean, quantity: Int, available:Boolean, status: Status, vendor_id: Int, min_stock_level: Int): Item
        
        saveTransaction(user_id: Int!, transaction_point_id: Int, vendor_id: Int!, total_amount: Float!, payment_type: String, payment_method: String, payment_detail: String, item_ids:[Int!]!, qtys:[Int!]!): Int!
   
        updateStock(id:Int!, qty_to_add:Int!, user_id:Int!): [Int]
        
        createShift(user_id:Int): Boolean! 
        
        endShift(user_id:Int): Boolean!
        
        resetPasswordAdmin(user_id:Int):Boolean!
        
        changePassword(user_id:Int!, old: String!, newP: String!, conf:String!):Boolean!
        
    }
    
    schema {
      query: Query
      mutation: Mutation
      subscription: Subscription
    }
`;
