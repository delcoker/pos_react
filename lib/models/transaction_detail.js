const my_variables = require('./del_index'); // just del trying this out

export default (sequelize, DataTypes) => {
    const TransactionDetail = sequelize.define('transaction_detail_', {
        quantity: DataTypes.INTEGER,
        // vendor_id: DataTypes.STRING,
        // status: DataTypes.ENUM(my_variables.statuses)
    });

    // I used hasMany but no associations were made i.e. Item hasMany TransactionDetails
    TransactionDetail.associate = (models) => {
        TransactionDetail.belongsTo(models.Item,{
            foreignKey: 'item_id'
        });
    };

    return TransactionDetail;
};
