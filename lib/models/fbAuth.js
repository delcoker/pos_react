export default (sequelize, DataTypes) => {
    const FbAuth = sequelize.define('fb_auth_', {
        fb_id: {
            type: DataTypes.STRING(100),
            // unique: true
        },
        username: DataTypes.STRING,
        display_name: DataTypes.STRING,
    });

    FbAuth.associate = (models) => {
        // 1 to many with Transaction
        FbAuth.belongsTo(models.User, {
            foreignKey: 'user_id',
        });
    }

    return FbAuth;
};
