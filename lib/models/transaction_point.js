const my_variables = require('./del_index'); // just del trying this out

export default (sequelize, DataTypes) => {
    const TransactionPoint = sequelize.define('transaction_point_', {
        name: DataTypes.STRING,
        // vendor_id: DataTypes.INTEGER,
        status: DataTypes.ENUM(my_variables.statuses)
    });

    TransactionPoint.associate = (models) => {
        // 1 to many with Transaction
        TransactionPoint.hasMany(models.Transaction, {
            foreignKey: 'transaction_point_id',
        });
    }

    return TransactionPoint;
};