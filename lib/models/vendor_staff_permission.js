const my_variables = require('./del_index'); // just del trying this out

export default (sequelize, DataTypes) => {
    const VendorStaffPerm = sequelize.define('vendor_staff_permission_', {
        name: DataTypes.STRING,
        status: DataTypes.ENUM(my_variables.statuses),
        can_add_vendor_staff: DataTypes.BOOLEAN,
        can_edit_vendor_staff: DataTypes.BOOLEAN,
        can_remove_vendor_staff: DataTypes.BOOLEAN,
        can_add_item: DataTypes.BOOLEAN,
        can_edit_item: DataTypes.BOOLEAN,
        can_remove_item: DataTypes.BOOLEAN,
    });

    VendorStaffPerm.associate = (models) => {
        // 1 to many with Transaction
        // Vendor.hasMany(models.Item, {
        //     foreignKey: 'vendor_id',
        // });
        // Vendor.hasMany(models.ItemCategory, {
        //     foreignKey: 'vendor_id',
        // });
        // VendorStaffPerm.hasMany(models.Transaction, {
        //     foreignKey: 'vendor_id',
        // });

        //// serious problem... this is supposed to be vendor_staff_perm_id
        VendorStaffPerm.hasMany(models.User, {
            foreignKey: 'vendor_id',
        });
    }
    return VendorStaffPerm;
};
