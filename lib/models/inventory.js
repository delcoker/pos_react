export default (sequelize, DataTypes) => {
    const Inventory = sequelize.define('inventory_', {
        quantity_added: DataTypes.INTEGER,
        quantity_after_this_addition: DataTypes.INTEGER,
        remarks: DataTypes.STRING,
    });

    // hasMany puts the foreignKey in the Item
    // belongsTo puts the foreignKey in the Inventory

    // Inventory.associate = (models) => {
    //     // 1 to many with item
    //     Inventory.belongsTo(models.Item, {
    //         foreignKey: 'item_id',
    //     });
    // };
    Inventory.associate = (models) => {
        // 1 to many with item
        Inventory.belongsTo(models.User, {
            foreignKey: 'user_id',
        });
    };



    return Inventory;
};
