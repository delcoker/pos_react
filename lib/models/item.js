import {GraphQLNonNull} from "graphql";
import DateTime from '../scalars/DateTime'

const my_variables = require('./del_index'); // just del trying this out
// console.log(my_variables.statuses);

export default (sequelize, DataTypes) => {
    const Item = sequelize.define('item_', {
        name: DataTypes.STRING,
        price: DataTypes.DECIMAL(10,2),
        pic: DataTypes.STRING,
        // category_id: DataTypes.INTEGER,
        has_stock: DataTypes.BOOLEAN,
        quantity: DataTypes.INTEGER,
        min_stock_level: DataTypes.INTEGER,
        available: DataTypes.BOOLEAN,
        status: DataTypes.ENUM(my_variables.statuses),
      /*  createdAt: {
            type: DateTime,
        },
        updatedAt: {
            type: DateTime
        },*/
    });

    Item.associate = (models) => {
        // 1 to many with TransactionDeatails
        Item.hasMany(models.TransactionDetail, {
            foreignKey: 'item_id',
        });
        Item.hasMany(models.Inventory, { // inventory has many items
            foreignKey: 'item_id', // will put item_id in Inventory table
        });

        // had to do this after for association to be created for queries
        Item.belongsTo(models.ItemCategory, {
            foreignKey: 'category_id',  // will put category_id in Item table
        });
        // Item.belongsTo(models.Inventory, {
        //     foreignKey: 'category_id',
        // });
    };

    return Item;
};
