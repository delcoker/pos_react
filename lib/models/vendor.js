const my_variables = require('./del_index'); // just del trying this out

export default (sequelize, DataTypes) => {
    const Vendor = sequelize.define('vendor_', {
        name: DataTypes.STRING,
        postal_address: DataTypes.TEXT,
        email: DataTypes.STRING(100),
        telephone: DataTypes.STRING,
        website_url: DataTypes.STRING,
        status: DataTypes.ENUM(my_variables.statuses)
    });

    Vendor.associate = (models) => {
        // 1 to many with Transaction
        Vendor.hasMany(models.Item, {
            foreignKey: 'vendor_id',
        });
        Vendor.hasMany(models.ItemCategory, {
            foreignKey: 'vendor_id',
        });
        Vendor.hasMany(models.Transaction, {
            foreignKey: 'vendor_id',
        });
        Vendor.hasMany(models.TransactionPoint, {
            foreignKey: 'vendor_id',
        });
        Vendor.hasMany(models.User, {
            foreignKey: 'vendor_id',
        });
    }

    return Vendor;
};