SELECT "transaction_"."user_id",
       "transaction_"."vendor_id",
       sum("transaction_detail_s"."quantity")               AS "transaction_detail_s.qty_sold",
       "transaction_detail_s->item_"."id"                   AS "transaction_detail_s.item_.id",
       "transaction_detail_s->item_"."name"                 AS "transaction_detail_s.item_.name",
       "transaction_detail_s->item_"."price"                AS "transaction_detail_s.item_.price",
       "transaction_detail_s->item_"."quantity"             AS "transaction_detail_s.item_.quantity",
       "transaction_detail_s->item_->item_category_"."id"   AS "transaction_
detail_s.item_.item_category_.id",
       "transaction_detail_s->item_->item_category_"."name" AS "transaction_detail_s.item_.item_category_.name"
FROM "transaction_s" AS "transaction_"
         LEFT OUTER JOIN "transaction_detail_s" AS "transaction_detail_s"
                         ON "transaction_"."id" = "transaction_detail_s"."transaction_id"
         LEFT OUTER JOIN "item_s" AS "transaction_detail_s->item_"
                         ON "transaction_detail_s"."item_id" = "transaction_detail_s->item_"."id"
         LEFT OUTER JOIN "item_category_s" AS "transaction_detail_s->item_->item_category_"
                         ON "transaction_detail_s->item_"."category_id" =
                            "transaction_detail_s->item_->item_category_"."id"
WHERE "transaction_"."user_id" = 1
  AND "transaction_"."createdAt" BETWEEN '2019-12-05 10:40:00.000 +00:00' AND '2019-12-05 23:40:48.525 +00:00'
GROUP BY "transaction_detail_s->item_"."id", "transaction_detail_s->item_"."name", "user_id",
         "transaction_"."vendor_id",
         "transaction_detail_s->item_->item_category_"."id", "transaction_detail_s.item_.quantity"
