const keyss = require('./keys'); // just del trying this out


import express from 'express';
import bodyParser from 'body-parser';
import {graphqlExpress, graphiqlExpress} from 'graphql-server-express';
import {makeExecutableSchema} from 'graphql-tools';
import cors from 'cors';
import jwt from 'jsonwebtoken'
import {createServer} from 'http';
import {execute, subscribe} from 'graphql';
import {SubscriptionServer} from 'subscriptions-transport-ws';
import _ from 'lodash';

import typeDefs from './schema';
import resolvers from './resolvers'
import models from './models'
// import {refreshTokens} from './auth';
import DataLoader from 'dataloader';
import passport from 'passport';
import FacebookStrategy from 'passport-facebook';


const myGraphQLSchema = makeExecutableSchema({
    typeDefs,
    resolvers,
});

const SECRET = "kjkhbnvdgjhltr345667nmmbbcvdg";

// const myGraphQLSchema = // ... define or import your schema here!
const PORT = 4000;

const app = express();

passport.use(new FacebookStrategy({
        clientID: keyss.FACEBOOK_CLIENT_ID,
        clientSecret: keyss.FACEBOOK_CLIENT_SECRET,
        callbackURL: "https://b9ee224c.ngrok.io/auth/facebook/callback"
    },
    async (accessToken, refreshToken, profile, cb) => {
        // 2 cases
        // first time login
        // other times
        const {id, username, displayName} = profile;
        const fbUsers = await models.FbAuth.findAll({
            limit: 1,
            where: {fb_id: id},
        });
        console.log(fbUsers);
        console.log(profile);
        if (!fbUsers.length) {

            const user = await models.User.create({vendor_id: 1});
            const fbUser = await models.FbAuth.create({
                fb_id: id,
                display_name: displayName,
                username: username,
                user_id: user.user_id
            });
        }
        cb(null, {});

    }
));


app.use(passport.initialize());

app.get('/flogin',
    passport.authenticate('facebook'));

app.get('/auth/facebook/callback',
    passport.authenticate('facebook', {session: false}),
    (req, res) => {
        res.send('Auth Was Good!');
        // Successful authentication, redirect home.
        // res.redirect('/');
    });

const addUser = async (req) => {
    // console.log(req.headers);

    const token = req.headers.authorization;
    // const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJfaWQiOjksImVtYWlsIjoiYUBhLmNvbSJ9LCJpYXQiOjE1NzAxOTcwOTgsImV4cCI6MTYwMTc1NDY5OH0.qAwfGHfVYSz1IUA-L7rTHcKihEQ8K-TWe7EesEZ0I5Y";
    // console.log("igheeeeeeeeeeeeeeeeeeeeeerrrrrrrrrrrrrrrrrrrrrreeeeeeeeeeeeeeeeeeeeeeeeee");
    try {
        const {user} = await jwt.verify(token, SECRET);
        // console.log('userrrrrrr', user);
        req.user = user;
    } catch (err) {
        console.log(err);
    }
    req.next();
};

const corsOptions = {
    origin: '*', //change with your own client URL
    credentials: true
};

app.use(cors(corsOptions));
app.use(addUser);

app.use(
    '/graphiql',
    graphiqlExpress({
        endpointURL: '/graphql',
    }),
);

// good practice to put this in another file/folder
const batchTransactionDetails = async (keys, {TransactionDetail}) => {
    const transactionDetails = await TransactionDetail.findAll({
        logging: console.log("-----------", typeof keys, keys),
        raw: true,
        where: {
            transaction_id: keys, //------------ make the keys and integer
        },
    });

    // console.log('transactionDetails', transactionDetails)

    const groupedTransactionDetails = _.groupBy(transactionDetails, 'transaction_id');

    return keys.map(k => groupedTransactionDetails[k] || []);
};

// bodyParser is needed just for POST.
app.use('/graphql',
    bodyParser.json(),
    graphqlExpress(req => ({
        schema: myGraphQLSchema,
        context: {
            models,
            SECRET,
            user: req.user,
            transactionDetailLoader: new DataLoader(keys => batchTransactionDetails(keys, models)),
        },
    })),
);

// del_seed();

app.use(express.static('client'));
app.use('*',express.static('client'));


// const pubsub = new PubSub();
const server = createServer(app);

models.sequelize.sync({force: false}).then(() =>
        server.listen(PORT, () => {
            new SubscriptionServer({
                execute,
                subscribe,
                schema: myGraphQLSchema,
            }, {
                server: server,
                path: '/subscriptions',
            });
        }),
    // del_seed()
);

// app.on('listening', function () {
//     // server ready to accept connections here
//     del_seed;
// });
