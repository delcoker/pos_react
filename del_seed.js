import models from "./models";

//////////////////////// Del hack //should use seeders/////////////////////////
export const del_seed = async () => {
    const akorno_vend = await models.Vendor.findOne({
        where: {id: 1}
    });
    // console.log("hereeeeeeeeeeeeeeeeeeee-------------eeeeeeeee");
    // console.log(akorno_vend);
    if (!akorno_vend) { //Del code for akorno -
        await models.Vendor.create({
            // id: 1,
            name: "Akorno Services Ltd.",
            postal_address: 'P.O. Box CT9329 Cantonemnts, Accra',
            email: 'beduaedusei@yahoo.com',
            telephone: '+233 (0)50 248 0435',
            website_url: 'www.gmail.com',
            status: 'enabled',
        });
        await models.User.bulkCreate([
            {
                first_name: "Kingston",
                last_name: 'Coker',
                isAdmin: true,
                vendor_id: 1,
                status: "enabled",
            },
            {
                first_name: "Bedua",
                last_name: 'Amenu',
                isAdmin: true,
                vendor_id: 1,
                status: "enabled",
            },
            {
                first_name: "Not",
                last_name: 'Admin',
                isAdmin: false,
                vendor_id: 1,
                status: "enabled"
            }
        ]);
        await models.LocalAuth.bulkCreate([
            {
                email: "delcoker@gmail.com",
                password: '$2b$12$D120oJDK/vleCaiykMJKw.0iLR.0YcW76iOReoAFiFfzzWMMmmu5G',
                user_id: 1,
                first_name: 'Kingston',
                last_name: 'Coker',
                other_names: 'DK',
                postal_address: '37 Portage Ave\nNorth York, ON\nM9N 3G8',
                telephone: '416-916-0196',
                pic: 'kingston coker',
                isAdmin: true,
                status: 'enabled',
                vendor_id: 1

    },
            {
                email: "beduaedusei@yahoo.com",
                password: '$2b$12$D120oJDK/vleCaiykMJKw.0iLR.0YcW76iOReoAFiFfzzWMMmmu5G',
                user_id: 2
            },
            {
                email: "noadmin@yahoo.com",
                password: '$2b$12$D120oJDK/vleCaiykMJKw.0iLR.0YcW76iOReoAFiFfzzWMMmmu5G',
                user_id: 3
            }
        ]);
        await models.ItemCategory.bulkCreate([
            {
                name: "Breakfast",
                status: 'enabled',
                vendor_id: 1
            },
            {
                name: "Singles",
                status: 'enabled',
                vendor_id: 1
            },
            {
                name: "Classic",
                status: 'enabled',
                vendor_id: 1
            },
            {
                name: "Deluxe",
                status: 'enabled',
                vendor_id: 1
            },
            {
                name: "Snack",
                status: 'enabled',
                vendor_id: 1
            },
            {
                name: "Staff Discount",
                status: 'enabled',
                vendor_id: 1
            }
        ]);
        await models.Item.bulkCreate([
            {
                name: "Pancakes",
                price: 1.50,
                pic: 'pancakes',
                has_stock: false,
                quantity: null,
                available: true,
                status: 'enabled',
                category_id: 1,
                vendor_id: 1,
                // min_stock_level: 1,
            },
            {
                name: "Apples",
                price: 4.0,
                pic: 'apples',
                has_stock: true,
                quantity: 10,
                available: true,
                status: 'enabled',
                category_id: 2,
                vendor_id: 1,
                min_stock_level: 1,
            },
            {
                name: "Plantain",
                price: 1.0,
                pic: 'plantain',
                has_stock: false,
                quantity: null,
                available: true,
                status: 'enabled',
                category_id: 2,
                vendor_id: 1,
            },
            {
                name: "Waakye",
                price: 6.50,
                pic: 'waakye',
                has_stock: false,
                quantity: null,
                available: true,
                status: 'enabled',
                category_id: 4,
                vendor_id: 1
            },
            {
                name: "Coke",
                price: 3,
                pic: 'coke',
                has_stock: true,
                quantity: 55,
                available: true,
                status: 'enabled',
                category_id: 5,
                vendor_id: 1,
                min_stock_level: 1,
            },
            {
                name: "Lucozade",
                price: 4.0,
                pic: 'Lucozade',
                has_stock: true,
                quantity: null,
                available: true,
                status: 'enabled',
                category_id: 5,
                vendor_id: 1,
                min_stock_level: 1,
            },
            {
                name: "Fish",
                price: 6.50,
                pic: 'Fish',
                has_stock: false,
                quantity: null,
                available: true,
                status: 'enabled',
                category_id: 3,
                vendor_id: 1
            },
            {
                name: "Chicken",
                price: 6.50,
                pic: 'chicken',
                has_stock: false,
                quantity: null,
                available: true,
                status: 'enabled',
                category_id: 4,
                vendor_id: 1
            },
            {
                name: "Goat",
                price: 6.50,
                pic: 'goat',
                has_stock: false,
                quantity: null,
                available: true,
                status: 'enabled',
                category_id: 4,
                vendor_id: 1
            },
            {
                name: "Cow",
                price: 6.50,
                pic: 'cow',
                has_stock: false,
                quantity: null,
                available: true,
                status: 'enabled',
                category_id: 2,
                vendor_id: 1
            },
            {
                name: "Chilli",
                price: .5,
                pic: 'chilli',
                has_stock: false,
                quantity: null,
                available: true,
                status: 'enabled',
                category_id: 5,
                vendor_id: 1
            },
            {
                name: "Small bottle water",
                price: 1.5,
                pic: 'small bottle water',
                has_stock: true,
                quantity: 200,
                available: true,
                status: 'enabled',
                category_id: 2,
                vendor_id: 1,
                min_stock_level: 50,
            },
            {
                name: "Big bottle water",
                price: 3,
                pic: 'big bottle watter',
                has_stock: true,
                quantity: 100,
                available: true,
                status: 'enabled',
                category_id: 2,
                vendor_id: 1,
                min_stock_level: 20,
            },
            {
                name: "Soft Drink",
                price: 4,
                pic: 'soft drink',
                has_stock: true,
                quantity: 50,
                available: true,
                status: 'enabled',
                category_id: 5,
                vendor_id: 1,
                min_stock_level: 1,
            },
            {
                name: "Tilapia",
                price: 12,
                pic: 'tilapia',
                has_stock: false,
                quantity: null,
                available: true,
                status: 'enabled',
                category_id: 4,
                vendor_id: 1
            },
            {
                name: "Open Till",
                price: 0.01,
                pic: 'tilapia',
                has_stock: false,
                quantity: null,
                available: true,
                status: 'enabled',
                category_id: 6,
                vendor_id: 1
            },
            {
                name: "Sausages",
                price: 6.5,
                pic: 'sausages',
                has_stock: false,
                quantity: null,
                available: true,
                status: 'enabled',
                category_id: 4,
                vendor_id: 1
            },
        ]);
        await models.TransactionPoint.create({
            name: "Main",
            vendor_id: 1,
            status: 'enabled',
        });
    }

    // req.next();
};

// app.on('listening', function () {
//     // server ready to accept connections here
// del_seed();
// });


//////////////////////////////////////////////////////
