const my_variables = require('./del_index'); // just del trying this out
'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {


      return queryInterface.bulkInsert('Vendor', [{
        name: "Akorno Catering Services",
        email: "delcoker@gmail.com",
        postal_address: "P.O Box CT9329 Cantonments, Accra",
        telephone: "+233 (0)50 248 0435",
        website_url: "akorno@gmail.com",
        status: my_variables.enabled

      }], {});

  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
