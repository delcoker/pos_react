import {Sequelize} from 'sequelize';
// import user_test from "./user";

// var basename  = path.basename(__filename);
// var env       = process.env.NODE_ENV || 'development';
// var config    = require(__dirname + '/../config/config.js')[env];

// server surface is password = 333
// server msi is password = 123

const sequelize = new Sequelize(
    'test_graphql_db',
    'test_graphql_user',
    '123', {
        host: 'localhost',
        dialect: 'postgres'
    });

// POSTGRES: root
// 12:     : 333

// create db tables
const db = {
    FbAuth: sequelize.import('./fbAuth'),
    LocalAuth: sequelize.import('./localAuth'),
    User: sequelize.import('./user'),
    Inventory: sequelize.import('./inventory'),
    Item: sequelize.import('./item'),
    ItemCategory: sequelize.import('./item_category'),
    Transaction: sequelize.import('./transaction'),
    TransactionDetail: sequelize.import('./transaction_detail'),
    TransactionPoint: sequelize.import('./transaction_point'),
    Vendor: sequelize.import('./vendor'),
    VendorStaffPerm: sequelize.import('./vendor_staff_permission'),

    Shift: sequelize.import('./shift'),
    ShiftDetail: sequelize.import('./shift_detail'),
};


//////////////////////// Del hack //should use seeders/////////////////////////
// const del_seed = async () => {
//     const akorno_vend = await models.Vendor.findOne({
//         where: {id: 1}
//     });
//     console.log("here");
//     console.log(akorno_vend);
//     if (!akorno_vend) { //Del code for akorno -
//         await models.Vendor.create({
//             id: 1,
//             name: "Akorno Services Ltd.",
//             postal_address: 'P.O. Box CT9329 Cantonemnts, Accra',
//             email: 'beduaedusei@yahoo.com',
//             telephone: '+233 (0)50 248 0435',
//             website_url: 'www.gmail.com',
//             status: 'enabled',
//         });
//     }
//     // req.next();
// };

// app.on('listening', function () {
//     // server ready to accept connections here
// del_seed();
// });


//////////////////////////////////////////////////////
// if (config.use_env_variable) {
//     var sequelize = new Sequelize(process.env[config.use_env_variable], config);
// } else {
//     var sequelize = new Sequelize(config.database, config.username, config.password, config);
// }

// fs.readdirSync(__dirname)
//     .filter(file => {
//         return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
//     })
//     .forEach(file => {
//         var model = sequelize['import'](path.join(__dirname, file));
//         db[model.name] = model;
//     });


// to actually add associations
Object.keys(db).forEach((modelName) => {
    if ('associate' in db[modelName]) {
        db[modelName].associate(db);
    }
});

db.sequelize = sequelize;
// db.Sequelize = Sequelize;

export default db;
