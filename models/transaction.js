export default (sequelize, DataTypes) => {
    const Transaction = sequelize.define('transaction_', {
        total_amount: DataTypes.DECIMAL(10,2),
        payment_type: DataTypes.STRING,
        // subscriber_id: DataTypes.STRING,
        payment_method: DataTypes.STRING,
        payment_detail: DataTypes.STRING,
        // meal_plan_id: DataTypes.INTEGER,
        // vendor_id: DataTypes.INTEGER,
        // meal_plan_group_id: DataTypes.INTEGER,
        // transaction_point_id: DataTypes.INTEGER,
    });

    Transaction.associate = (models) => {

        Transaction.hasMany(models.TransactionDetail, {
            foreignKey: 'transaction_id',
        });
    };

    return Transaction;
};