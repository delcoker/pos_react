export default (sequelize, DataTypes) => {
    const LocalAuth = sequelize.define('local_auth_', {
        email: {
            type: DataTypes.STRING(100),
            unique: true
        },
        password: DataTypes.STRING,
    });

    LocalAuth.associate = (models) => {
        // 1 to many with Transaction
        LocalAuth.belongsTo(models.User, {
            foreignKey: 'user_id',
        });
    }

    return LocalAuth;
};
