const my_variables = require('./del_index'); // just del trying this out

export default (sequelize, DataTypes) => {
    const ItemCategory = sequelize.define('item_category_', {
        name: DataTypes.STRING,
        // vendor_id: DataTypes.INTEGER,
        status: DataTypes.ENUM(my_variables.statuses),
    });

    ItemCategory.associate = (models) => {
        // 1 to many with item
        ItemCategory.hasMany(models.Item, {
            foreignKey: 'category_id',
        });
        // ItemCategory.belongsTo(models.Item, {
        //     foreignKey: 'category_id',
        // });
    };

    return ItemCategory;
};
