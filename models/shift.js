import * as my_variables from "./del_index";

export default (sequelize, DataTypes) => {
    const Shift = sequelize.define('shift_', {
        // user_id: DataTypes.STRING,
        // active_shift: DataTypes.BOOLEAN,
        status: DataTypes.ENUM(my_variables.statuses),
    });

    Shift.associate = (models) => {

        Shift.belongsTo(models.User, {
            foreignKey: 'user_id',
        });

        // 1 to many with ShiftDetail
        Shift.hasMany(models.ShiftDetail, {
            foreignKey: 'shift_id',
        });
    };

    return Shift;
};
