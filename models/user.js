export default (sequelize, DataTypes) => {
    const User = sequelize.define('user_', {
        user_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        first_name: DataTypes.STRING,
        last_name: DataTypes.STRING,
        other_names: DataTypes.STRING,
        postal_address: DataTypes.TEXT,
        telephone: DataTypes.STRING,
        /* local auth takes care of this*/
        // email: {
        //     type: DataTypes.STRING(100),
        //     unique: true
        // },
        // password: DataTypes.STRING,
        pic: DataTypes.STRING,
        isAdmin: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        // vendor_id: DataTypes.STRING,
        // vendor_staff_perm_id: DataTypes.STRING,
        status:{
            type: DataTypes.ENUM('enabled', 'disabled', 'deleted'),
            defaultValue: "disabled"
        }
    });

    User.associate = (models) => {
        // 1 to many with Transaction
        User.hasMany(models.Transaction, {
            foreignKey: 'user_id',
        });
    };

    return User;
};
