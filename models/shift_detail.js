import {GraphQLNonNull} from "graphql";
import DateTime from '../scalars/DateTime'

const my_variables = require('./del_index'); // just del trying this out
// console.log(my_variables.statuses);

export default (sequelize, DataTypes) => {
    const ShiftDetail = sequelize.define('shift_detail_', {
        // user_id: DataTypes.STRING,
        // item_id: DataTypes.DECIMAL(10,2),
        qty_start: DataTypes.INTEGER,
        qty_during_shift: DataTypes.INTEGER,
        qty_end: DataTypes.INTEGER,
    });

    ShiftDetail.associate = (models) => {
        // had to do this for association to be created for queries
        ShiftDetail.belongsTo(models.Item,{
            foreignKey: 'item_id'
        });
        // ShiftDetail.belongsTo(models.Shift,{
        //     foreignKey: 'shift_id'
        // });
    };

    return ShiftDetail;
};
